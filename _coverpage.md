<div class="vibe-logo">
  <a href="https://www.vibetecnologia.com/" target="_blank">
    <div class="logo"></div>
  </a>
</div>

![logo](_media/_logo.png ":size=20%")

> Integrador de mensagens via WhatsApp

- 🚀 Rápido e confiável!
- 💰️ Mais barato que os demais serviços
- 📱 Múltiplos números em uma única plataforma
- 🇧🇷 Solução nacional, com suporte dedicado em Português

<div class="buttons">
  <a href="#/README"><span>Começar a usar</span></a>
</div>

![color](#ffffff)
