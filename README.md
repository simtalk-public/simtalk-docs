# O que é o {{ APP_NAME }}?

O {{ APP_NAME }} é um integrador WhatsApp projetado para utilizar o máximo das tecnologias de computação em nuvem, possibilitando a você uma ferramenta confiável e rápida para troca de mensagens via WhatsApp.

Aqui no {{ APP_NAME }} acreditamos que os clientes não precisam decidir entre qualidade e custo para contratar uma solução. Por isso, não existem segredos: o preço do {{ APP_NAME }} é fixo e previsível, não importando quantas mensagens você envie ou receba em um determinado mês.

Além disso, temos um time de suporte dedicado para resolver todos os problemas que você possa ter, que fala sua lingua!

## O que eu posso fazer com o {{ APP_NAME }}?

- 📱 Conectar múltiplos números de WhatsApp
- 📨 Envio e recebimento de mensagens em tempo real
- 📎 Envio e recebimento de arquivos até 20mb
- 🔊 Envio e recebimento de mensagens de voz

## Como utilizo?

Para utilizar o {{ APP_NAME }} é muito simples! Você não precisa instalar nenhum software em seu computador ou celular, basta que você siga os passos abaixo e você poderá utilizar a API {{ APP_NAME }} agora mesmo!

1. O primeiro passo é criar uma conta! [Clique aqui e descubra como criar uma conta](pages/nova_conta.md).
2. Já possui uma conta? Então [aprenda a se autenticar](autenticacao.md) e [conectar o {{ APP_NAME }} com o WhatsApp](pages/conectando.md).
3. A partir daí, é super simples [enviar](pages/enviando_mensagens.md) e [receber](pages/recebendo_mensagens.md) mensagens!
4. E se surgir alguma dúvida ou problema, basta contatar nosso time de [suporte](pages/suporte.md).

## É seguro utilizar?

A utilização de forma consciente da ferramenta é segura, e trabalhamos muito para manter desse jeito. Porém, é importante deixar claro alguns pontos:

> <b>ATENÇÃO⚠️</b> <br/>
> Caso você envie _MUITAS_ mensagens em um curto intervalo de tempo para números que _NÃO SÃO_ seus contatos, seu WhatsApp corre risco de ser bloqueado! <br/>
> Para evitar esses problemas, confira [as nossas dicas](pages/riscos.md)
