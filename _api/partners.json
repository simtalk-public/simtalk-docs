{
  "openapi": "3.0.0",
  "paths": {
    "/core/session/{accountId}": {
      "get": {
        "operationId": "CoreController_getSessionInfo",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "200": {
            "description": "Return session info",
            "content": {
              "application/json": {
                "schema": { "$ref": "#/components/schemas/SessionInfoDto" }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized." },
          "500": { "description": "Forbidden." }
        },
        "tags": ["Core"],
        "security": [{ "bearer": [] }]
      },
      "post": {
        "operationId": "CoreController_startSession",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "204": { "description": "Start whatsapp session" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized." },
          "500": { "description": "Forbidden." }
        },
        "tags": ["Core"],
        "security": [{ "bearer": [] }]
      },
      "delete": {
        "operationId": "CoreController_destroySession",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "204": { "description": "Destroy whatsapp session" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized." },
          "500": { "description": "Forbidden." }
        },
        "tags": ["Core"],
        "security": [{ "bearer": [] }]
      }
    },
    "/core/session/{accountId}/restart": {
      "post": {
        "operationId": "CoreController_restartSession",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "204": { "description": "Restart whatsapp session" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized." },
          "500": { "description": "Forbidden." }
        },
        "tags": ["Core"],
        "security": [{ "bearer": [] }]
      }
    },
    "/auth/login": {
      "post": {
        "operationId": "AuthController_login",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/LoginUserDto" }
            }
          }
        },
        "responses": { "500": { "description": "Internal server error" } },
        "tags": ["Auth"]
      }
    },
    "/auth/partners/login": {
      "post": {
        "operationId": "AuthController_partnersLogin",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/LoginUserDto" }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Returns authentication token",
            "content": {
              "application/json": {
                "schema": { "$ref": "#/components/schemas/LoginResponseDTO" }
              }
            }
          },
          "500": { "description": "Internal server error" }
        },
        "tags": ["Auth"]
      }
    },
    "/auth/logout": {
      "post": {
        "operationId": "AuthController_logout",
        "parameters": [],
        "responses": { "500": { "description": "Internal server error" } },
        "tags": ["Auth"]
      }
    },
    "/partner": {
      "post": {
        "operationId": "PartnersController_create",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/CreateCompanyDto" }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Create a new company",
            "content": {
              "application/json": {
                "schema": { "$ref": "#/components/schemas/PartnerCompanyDto" }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Partners"],
        "security": [{ "bearer": [] }]
      },
      "get": {
        "operationId": "PartnersController_getPartnerInfo",
        "parameters": [],
        "responses": {
          "200": {
            "description": "Fetches partner info",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": { "$ref": "#/components/schemas/PartnerCompanyDto" }
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" }
        },
        "tags": ["Partners"],
        "security": [{ "bearer": [] }]
      }
    },
    "/partner/{companyId}/accounts": {
      "get": {
        "operationId": "PartnersController_getAccounts",
        "parameters": [
          {
            "name": "companyId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "200": {
            "description": "Find all accounts of a company",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": { "$ref": "#/components/schemas/AccountDto" }
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" }
        },
        "tags": ["Partners"],
        "security": [{ "bearer": [] }]
      }
    },
    "/partner/settings": {
      "post": {
        "operationId": "PartnersController_updateSettings",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/PartnerSettingsDto" }
            }
          }
        },
        "responses": {
          "204": { "description": "Updates callback url" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Partners"],
        "security": [{ "bearer": [] }]
      }
    },
    "/accounts/{id}": {
      "get": {
        "operationId": "PartnerAccountsController_findOne",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "200": {
            "description": "Return an account",
            "content": {
              "application/json": {
                "schema": { "$ref": "#/components/schemas/AccountDto" }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Accounts"],
        "security": [{ "bearer": [] }]
      },
      "patch": {
        "operationId": "PartnerAccountsController_update",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/UpdateAccountDto" }
            }
          }
        },
        "responses": {
          "200": { "description": "Update and return updated account" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Accounts"],
        "security": [{ "bearer": [] }]
      },
      "delete": {
        "operationId": "PartnerAccountsController_remove",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "204": { "description": "Delete an account" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Accounts"],
        "security": [{ "bearer": [] }]
      }
    },
    "/accounts/restart/{id}": {
      "post": {
        "operationId": "PartnerAccountsController_restart",
        "parameters": [
          {
            "name": "id",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "204": { "description": "Delete an account" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Accounts"],
        "security": [{ "bearer": [] }]
      }
    },
    "/accounts": {
      "post": {
        "operationId": "PartnerAccountsController_addAccount",
        "parameters": [],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": { "$ref": "#/components/schemas/CreateAccountDto" }
            }
          }
        },
        "responses": {
          "201": { "description": "Create and return a new account" },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Accounts"],
        "security": [{ "bearer": [] }]
      }
    },
    "/contacts/{accountId}/{contactNumber}/messages": {
      "get": {
        "operationId": "PartnerContactsController_listMessages",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          },
          {
            "name": "contactNumber",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          },
          {
            "name": "limit",
            "required": false,
            "in": "query",
            "schema": { "type": "number" }
          },
          {
            "name": "date",
            "required": false,
            "in": "query",
            "schema": { "type": "number" }
          }
        ],
        "responses": {
          "200": {
            "description": "Returns contact messages",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": { "$ref": "#/components/schemas/ContactMessagesDto" }
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Contact"],
        "security": [{ "bearer": [] }]
      }
    },
    "/contacts/{accountId}/{contactNumber}/info": {
      "get": {
        "operationId": "PartnerContactsController_getContactInfo",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          },
          {
            "name": "contactNumber",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "responses": {
          "200": {
            "description": "Return contacts details",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/WhatsappContactInfoResponseDto"
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Contact"],
        "security": [{ "bearer": [] }]
      }
    },
    "/messages/{accountId}/text": {
      "post": {
        "operationId": "PartnerMessagesController_sendTextMessage",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/SendTextMessageRequestDto"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Sends a text message",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SendMessageResponseDto"
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Messages"],
        "security": [{ "bearer": [] }]
      }
    },
    "/messages/{accountId}/audio": {
      "post": {
        "operationId": "PartnerMessagesController_sendAudioMessage",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/SendAudioMessageRequestDto"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Sends an audio message",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SendMessageResponseDto"
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Messages"],
        "security": [{ "bearer": [] }]
      }
    },
    "/messages/{accountId}/file": {
      "post": {
        "operationId": "PartnerMessagesController_sendFileMessage",
        "parameters": [
          {
            "name": "accountId",
            "required": true,
            "in": "path",
            "schema": { "type": "string" }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/SendMediaMessageRequestDto"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Sends a file message",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SendMessageResponseDto"
                }
              }
            }
          },
          "400": { "description": "Bad request" },
          "401": { "description": "Unauthorized" },
          "500": { "description": "Internal Server Error" }
        },
        "tags": ["Messages"],
        "security": [{ "bearer": [] }]
      }
    }
  },
  "info": {
    "title": "{{ APP_NAME }}",
    "description": "{{ APP_NAME }} API documentation",
    "version": "1.0",
    "contact": {}
  },
  "tags": [{ "name": "{{ APP_NAME }}", "description": "" }],
  "servers": [],
  "components": {
    "securitySchemes": {
      "bearer": { "scheme": "bearer", "bearerFormat": "JWT", "type": "http" }
    },
    "schemas": {
      "SessionInfoDto": {
        "type": "object",
        "properties": {
          "clientPushName": { "type": "string" },
          "profilePicUrl": { "type": "string" },
          "clientNumber": { "type": "string" }
        },
        "required": ["clientPushName", "profilePicUrl", "clientNumber"]
      },
      "LoginUserDto": {
        "type": "object",
        "properties": {
          "email": { "type": "string" },
          "password": { "type": "string" }
        },
        "required": ["email", "password"]
      },
      "LoginResponseDTO": {
        "type": "object",
        "properties": { "token": { "type": "string" } },
        "required": ["token"]
      },
      "CreateOwnerDto": {
        "type": "object",
        "properties": {
          "email": { "type": "string" },
          "password": { "type": "string" },
          "name": { "type": "string" },
          "phoneNumber": { "type": "string" }
        },
        "required": ["email", "password", "name", "phoneNumber"]
      },
      "CreateCompanyDto": {
        "type": "object",
        "properties": {
          "companyName": { "type": "string" },
          "owner": { "$ref": "#/components/schemas/CreateOwnerDto" }
        },
        "required": ["companyName", "owner"]
      },
      "PartnerCompanyDto": {
        "type": "object",
        "properties": {
          "id": { "type": "string" },
          "name": { "type": "string" },
          "users": { "type": "array", "items": { "type": "string" } },
          "accounts": { "type": "array", "items": { "type": "string" } }
        },
        "required": ["id", "name", "users", "accounts"]
      },
      "AccountDto": {
        "type": "object",
        "properties": {
          "id": { "type": "string" },
          "label": { "type": "string" },
          "status": { "type": "string" },
          "createdAt": { "format": "date-time", "type": "string" }
        },
        "required": ["id", "label", "status", "createdAt"]
      },
      "PartnerSettingsDto": {
        "type": "object",
        "properties": {
          "accountId": { "type": "string" },
          "eventsCallback": { "type": "string" },
          "messagesCallback": { "type": "string" }
        },
        "required": ["accountId", "eventsCallback", "messagesCallback"]
      },
      "UpdateAccountDto": {
        "type": "object",
        "properties": { "label": { "type": "string" } }
      },
      "CreateAccountDto": {
        "type": "object",
        "properties": {
          "companyId": { "type": "string" },
          "label": { "type": "string" }
        },
        "required": ["companyId", "label"]
      },
      "ContactMessagesDto": {
        "type": "object",
        "properties": {
          "id": { "type": "string" },
          "from": { "type": "string" },
          "fromMe": { "type": "boolean" },
          "body": { "type": "string" },
          "hasMedia": { "type": "boolean" },
          "isForwarded": { "type": "boolean" },
          "timestamp": { "type": "number" },
          "hasQuotedMessage": { "type": "boolean" },
          "quotedMessageId": { "type": "string" },
          "media": { "type": "string" },
          "ack": { "type": "number" },
          "type": { "type": "number" }
        },
        "required": [
          "id",
          "from",
          "fromMe",
          "body",
          "hasMedia",
          "isForwarded",
          "timestamp",
          "hasQuotedMessage",
          "quotedMessageId",
          "media",
          "ack",
          "type"
        ]
      },
      "WhatsappContactInfoResponseDto": {
        "type": "object",
        "properties": {
          "pushName": { "type": "string" },
          "profilePicUrl": { "type": "string" },
          "about": { "type": "string" },
          "isMyContact": { "type": "boolean" },
          "savedContactName": { "type": "string" },
          "number": { "type": "string" }
        },
        "required": [
          "pushName",
          "profilePicUrl",
          "about",
          "isMyContact",
          "savedContactName",
          "number"
        ]
      },
      "SendTextMessageRequestDto": {
        "type": "object",
        "properties": {
          "number": { "type": "string" },
          "message": { "type": "string" }
        },
        "required": ["number", "message"]
      },
      "SendMessageResponseDto": {
        "type": "object",
        "properties": {
          "id": { "type": "string" },
          "status": { "type": "string" }
        },
        "required": ["id", "status"]
      },
      "SendAudioMessageRequestDto": {
        "type": "object",
        "properties": {
          "number": { "type": "string" },
          "fileb64": { "type": "string" },
          "mimetype": { "type": "string" }
        },
        "required": ["number", "fileb64", "mimetype"]
      },
      "SendMediaMessageRequestDto": {
        "type": "object",
        "properties": {
          "number": { "type": "string" },
          "fileb64": { "type": "string" },
          "mimetype": { "type": "string" },
          "caption": { "type": "string" },
          "filename": { "type": "string" }
        },
        "required": ["number", "fileb64", "mimetype", "caption", "filename"]
      }
    }
  }
}
