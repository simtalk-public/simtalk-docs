# Nova Conta

O primeiro passo para iniciar sua jornada no {{ APP_NAME }} é a criação de uma conta.

Uma conta identifica sua empresa, e concentra em uma única plataforma para você gerenciar todos os seus números conectados!

Já possui sua conta? Então o próximo passo é se [autenticar](/pages/autenticacao.md) no sistema!

> <b>ATENÇÃO⚠️</b> <br/>
> Quando você cadastra sua empresa, o {{ APP_NAME }} cria automaticamente uma conta capaz de se conectar com um número de WhatsApp para você <br/>
> Isso significa que seu período de testes será iniciado e, caso você não cancele sua conta, cobranças poderão ser realizadas mesmo que você deixe de utilizar o sistema!

<hr/>

## Método da API

`POST` {{BASE_URL}}/partner

### Parâmetros

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
        <th>
            Obrigatório
        </th>
    </tr>
    <tr>
        <td>
            companyName
        </td>
        <td>
            String
        </td>
        <td>
            Nome da sua empresa
        </td>
        <td>
            Sim
        </td>
    </tr>
    <tr>
        <td>
            owner
        </td>
        <td>
            <table>
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Descrição
                    </th>
                    <th>
                        Obrigatório
                    </th>
                </tr>
                <tr>
                    <td>
                        email
                    </td>
                    <td>
                        String
                    </td>
                    <td>
                        E-mail do responsável pela empresa
                    </td>
                    <td>
                        Sim
                    </td>
                </tr>
                <tr>
                    <td>
                        password
                    </td>
                    <td>
                        String
                    </td>
                    <td>
                        Senha de acesso
                    </td>
                    <td>
                        Sim
                    </td>
                </tr>
                <tr>
                    <td>
                        name
                    </td>
                    <td>
                        String
                    </td>
                    <td>
                        Nome do responsável pela empresa
                    </td>
                    <td>
                        Sim
                    </td>
                </tr>
                <tr>
                    <td>
                        phoneNumber
                    </td>
                    <td>
                        String
                    </td>
                    <td>
                        Telefone de contato do responsável pela empresa
                    </td>
                    <td>
                        Sim
                    </td>
                </tr>
            </table>
        </td>
        <td>
            Informações do usuário responsável
        </td>
        <td>
            Sim
        </td>
    </tr>
</table>

### Resposta

**HTTP Status 201**

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
    </tr>
    <tr>
        <td>
            id
        </td>
        <td>
            string
        </td>
        <td>
            Id da conta
        </td>
    </tr>
    <tr>
        <td>
            name
        </td>
        <td>
            string
        </td>
        <td>
            Nome da empresa cadastrado
        </td>
    </tr>
    <tr>
        <td>
            users[]
        </td>
        <td>
            <table>
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Descrição
                    </th>
                </tr>
            </table>
        </td>
        <td>
            Usuários vinculados à empresa
        </td>
    </tr>
    <tr>
        <td>
            accounts[]
        </td>
        <td>
            <table>
                <tr>
                    <th>
                        Nome
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Descrição
                    </th>
                </tr>
            </table>
        </td>
        <td>
            Contas vinculadas à empresa
        </td>
    </tr>
</table>

### Exemplo

<iframe src="//api.apiembed.com/?source=https://vibedesenv-my.sharepoint.com/:u:/g/personal/francisco_santana_vibetecnologia_com/EQQ7BKLkM0tAgk9P_mt1goYBiqS-fUdL5e16pRBxs4GNHA?e=Gq7lUJ&targets=shell:curl,node:unirest,java:unirest,python:requests,php:curl,ruby:native,objc:nsurlsession,go:native" frameborder="0" scrolling="no" width="100%" height="500px" seamless></iframe>
