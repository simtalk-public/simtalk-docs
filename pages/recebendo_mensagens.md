# Recebimento de Mensagens

O recebimento de mensagens ocorre de forma automática. Toda mensagem recebida no WhatsApp é encaminhada automaticamente para a URL `messagesCallback`. Caso você queira alterar a URL do callback de mensagens, deve utilizar o método abaixo.

### Método da API

**/partner**

`POST` {{BASE_URL}}/partner/settings

### Parâmetros

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
        <th>
            Obrigatório
        </th>
    </tr>
    <tr>
        <td>
            accountId
        </td>
        <td>
            String
        </td>
        <td>
            id da conta
        </td>
        <td>
            Sim
        </td>
    </tr>
    <tr>
        <td>
            eventsCallback
        </td>
        <td>
            String
        </td>
        <td>
            URL do callback de eventos (POST)
        </td>
        <td>
            Não
        </td>
    </tr>
    <tr>
        <td>
            messagesCallback
        </td>
        <td>
            String
        </td>
        <td>
            URL do callback de mensagens (POST)
        </td>
        <td>
            Não
        </td>
    </tr>
</table>

### Resposta

**HTTP Status 204**

<hr/>

## Mensagens recebidas

As mensagens recebidas chegam sempre por requisições do tipo `POST`. O conteúdo das mensagens pode ser encontrado no corpo da requisição.

```json
{
  "accountId": "string",
  "id": "string",
  "from": "string",
  "name": "string",
  "profilePicUrl": "string",
  "chat_id": "string",
  "timestamp": "string (unix timestamp)",
  "hasMedia": "boolean",
  "to": "string",
  "location": null,
  "contactCards": null,
  "body": "string",
  "media": "string",
  "type": "number"
}
```
