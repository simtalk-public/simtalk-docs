# Conectando com o WhatsApp

O passo mais aguardado de todos é sempre a conexão com o WhatsApp. Essa etapa inclui os seguintes passos:

1. Configuração do callback de eventos
2. Inicialização de uma conta
3. Obtenção e leitura do QR Code

> <b>ATENÇÃO⚠️</b> <br/>
> Todos os serviços listados abaixo necessitam autenticação. Não esqueça de incluir o cabeçalho <b>authorization</b> com seu token de autenticação!

## Configuração de callbacks

Para configurar as URLs de callback, você deve acionar o método abaixo da API. Os eventos para leitura de QR Code vão ser encaminhados para a URL do parâmetro eventsCallback

> Observe que na mesma requisição você pode alterar ambos o callback de mensagens e eventos. Os callbacks serão acionados por meio de métodos POST para a URL indicada no parâmetro

### Método da API

**/partner**

`POST` {{BASE_URL}}/partner/settings

### Parâmetros

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
        <th>
            Obrigatório
        </th>
    </tr>
    <tr>
        <td>
            accountId
        </td>
        <td>
            String
        </td>
        <td>
            id da conta
        </td>
        <td>
            Sim
        </td>
    </tr>
    <tr>
        <td>
            eventsCallback
        </td>
        <td>
            String
        </td>
        <td>
            URL do callback de eventos (POST)
        </td>
        <td>
            Não
        </td>
    </tr>
    <tr>
        <td>
            messagesCallback
        </td>
        <td>
            String
        </td>
        <td>
            URL do callback de mensagens (POST)
        </td>
        <td>
            Não
        </td>
    </tr>
</table>

### Resposta

**HTTP Status 204**

## Inicialização da Conta

O passo seguinte é inicializar a conta. No momento da inicialização, assim que o qr code for gerado ele será encaminhado para a URL do callback de eventos

### Método da API

**/partner**

`POST` {{BASE_URL}}/core/session/{ACCOUNT_Id}

### Parâmetros

`ACCOUNT_ID`: Identificador da conta

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
        <th>
            Obrigatório
        </th>
    </tr>
    <tr>
        <td>
            accountId
        </td>
        <td>
            String
        </td>
        <td>
            id da conta
        </td>
        <td>
            Sim
        </td>
    </tr>
</table>

> Esse método inicializa a sessão da conta passada por parâmetro. Caso sua conta já tenha sido inicializada, o método não terá nenhum efeito.

### Resposta

**HTTP Status 204**
