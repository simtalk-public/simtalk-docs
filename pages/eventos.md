# Eventos

O {{ APP_NAME }} dispara vários eventos para comunicação efetiva com outros sistemas. Todos os eventos podem ser encontrados abaixo.

## Tipos de Evento

### QR Code

#### ID do Evento: 0

#### Conteúdo do Evento:

```json
{
  "accountId": "string",
  "eventType": "1",
  "qrCode": "string"
}
```

<hr/>

### Autenticação bem sucedida

#### ID do Evento: 2

#### Conteúdo do Evento:

```json
{
  "accountId": "string",
  "eventType": "2",
  "status": "Authentication success!"
}
```

<hr/>

### Integração pronta para uso

#### ID do Evento: 1

#### Conteúdo do Evento:

```json
{
  "accountId": "string",
  "eventType": "1",
  "info": {
    "clientNumber": "string",
    "clientPushName": "string",
    "profilePicUrl": "string"
  }
}
```

<hr/>

### Status de Mensagem Atualizado

#### ID do Evento: 3

#### Conteúdo do Evento:

```json
{
  "accountId": "string",
  "eventType": "3",
  "messageId": "string",
  "dir": "string", // "i" for incoming
  "ack": "string" //
}
```

ACKs Table

<table>
    <tr>
        <th>
            ID
        </th>
        <th>
            Description
        </th>
    </tr>
    <tr>
        <td>-1</td>
        <td>Erro!</td>
    </tr>
    <tr>
        <td>0</td>
        <td>Pendente de envio</td>
    </tr>
    <tr>
        <td>1</td>
        <td>Mensagem enviada</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Mensagem recebida</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Mensagem lida</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Mensagem de voz escutada</td>
    </tr>
</table>
