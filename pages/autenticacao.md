# Autenticando

É necessário que você cadastre sua empresa para utilizar o {{APP_NAME}}. Caso você não o tenha feito ainda, veja como fazê-lo [aqui](/pages/nova_conta.md).

Caso você já possua uma conta, saiba que o e-mail de cadastro e sua senha são suas credenciais de acesso! Utilize o serviço abaixo para realizar a autenticação!

> <b>Autorização</b> <br/>
> Em todos os demais serviços que necessitem autenticação, você deveria incluir no cabeçalho da sua requisição o token de autenticação! Ele deve ser inserido através do cabeçalho <b>authorization</b>

<hr/>

## Método da API

`POST` {{BASE_URL}}/auth/login

### Parâmetros

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
        <th>
            Obrigatório
        </th>
    </tr>
    <tr>
        <td>
            email
        </td>
        <td>
            String
        </td>
        <td>
            E-mail de cadastro
        </td>
        <td>
            Sim
        </td>
    </tr>
    <tr>
        <td>
            password
        </td>
        <td>
            String
        </td>
        <td>
            Senha de cadastro
        </td>
        <td>
            Sim
        </td>
    </tr>
</table>

### Resposta

**HTTP Status 200**

<table>
    <tr>
        <th>
            Nome
        </th>
        <th>
            Tipo
        </th>
        <th>
            Descrição
        </th>
    </tr>
    <tr>
        <td>
            token
        </td>
        <td>
            String
        </td>
        <td>
            Token utilizado nos serviços que precisam de autenticação. 
        </td>
    </tr>
</table>
