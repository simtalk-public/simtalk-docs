# Enviando Mensagens

O envio de mensagens é realizado por quatro serviços, um para cada tipo: envio de mensagens de texto, de arquivos, de arquivos através de uma URL e de mensagens de voz.

## Envio de Mensagens de Texto

`POST` {{BASE_URL}}/messages/{ACCOUNT_ID}/text

### Parâmetros

```json
{
  "number": "string",
  "message": "string"
}
```

### Resposta

```json
{
  "status": "string",
  "id": "string"
}
```

## Envio de Arquivos

**/partner**

`POST` {{BASE_URL}}/messages/{ACCOUNT_ID}/file

### Parâmetros

```json
{
  "file": "string",
  "fileName": "string",
  "messageData": {
    "number": "string",
    "caption": "string",
    "mimetype": "string"
  }
}
```

### Resposta

```json
{
  "status": "string",
  "id": "string"
}
```

## Envio de Arquivos (URL)

**/partner**

`POST` {{BASE_URL}}/messages/{ACCOUNT_ID}/fileUrl

### Parâmetros

```json
{
  "fileUrl": "string",
  "messageData": {
    "number": "string",
    "caption": "string",
    "mimetype": "string"
  }
}
```

### Resposta

```json
{
  "status": "string",
  "id": "string"
}
```

## Envio de Mensagens de Voz

**/partner**

`POST` {{BASE_URL}}/messages/{ACCOUNT_ID}/audio

### Parâmetros

```json
{
  "file": "string",
  "messageData": {
    "number": "string",
    "caption": "string",
    "mimetype": "string"
  }
}
```

### Resposta

```json
{
  "status": "string",
  "id": "string"
}
```
